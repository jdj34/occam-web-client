#!/bin/bash
set -eu

mkdir -p ./vendor

pkgdir="$PWD/vendor"
pkgver=`cat .ruby-version`
source=https://cache.ruby-lang.org/pub/ruby/${pkgver:0:3}/ruby-${pkgver}.tar.xz

export LD_LIBRARY_PATH=$PWD/vendor/lib:${LD_LIBRARY_PATH-}
export PATH=$PWD/vendor/bin:$PWD/vendor/go/bin:${PATH-}

rootdir=$PWD

cd "$pkgdir"

if [ ! -d ruby-${pkgver} ]; then
  wget $source
  tar xvf ruby-${pkgver}.tar.xz
fi

cd ruby-${pkgver}
./configure \
    --prefix="${pkgdir}" \
    --enable-shared \
    --disable-rpath \
    --with-dbm-type=gdbm_compat
make
make install

cd $rootdir
