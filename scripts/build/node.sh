#!/bin/bash
set -eu

mkdir -p ./vendor

pkgver=12.10.0
pkgdir="$PWD/vendor"
source=https://github.com/nodejs/node

export LD_LIBRARY_PATH=$PWD/vendor/lib:${LD_LIBRARY_PATH-}
export PATH=$PWD/vendor/bin:${PATH-}

rootdir=$PWD

cd "$pkgdir"

if [ ! -d node ]; then
  git clone $source node
fi

cd node
git checkout tags/v${pkgver}
./configure \
    --prefix="${pkgdir}" || exit -1
make -j4 || exit -1
make install || exit -1

cd $rootdir
