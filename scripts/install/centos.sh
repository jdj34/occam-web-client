#!/bin/bash
set -eu

# Install required packages

# Package Dependencies
BINARY_PACKAGES=(make gcc pkg-config curl wget tcl)
BINARY_CHECK=(make gcc pkg-config curl wget tclsh)
LIBRARY_PACKAGES=()
LIBRARY_CHECK=()
INCLUDE_PACKAGES=(ncurses-devel openssl-devel bzip2-devel readline-devel libcurl-devel zlib-devel xz-devel)
INCLUDE_CHECK=(/usr/include/ncurses.h /usr/include/openssl/crypto.h /usr/include/bzlib.h /usr/include/readline/history.h /usr/include/curl/curl.h /usr/include/zlib.h /usr/include/lzma.h)

# Stores the packages we need to install
PACKAGES=()

for i in "${!BINARY_CHECK[@]}"; do
  binary=${BINARY_CHECK[$i]}
  package=${BINARY_PACKAGES[$i]}
  if hash ${binary} 2>/dev/null; then
    echo "${package} already installed"
  else
    PACKAGES+=(${package})
  fi
done

for i in "${!LIBRARY_CHECK[@]}"; do
  library=${LIBRARY_CHECK[$i]}
  package=${LIBRARY_PACKAGES[$i]}
  if pkg-config ${library} --libs 2>/dev/null >/dev/null; then
    echo "${package} already installed"
  else
    PACKAGES+=(${package})
  fi
done

for i in "${!INCLUDE_CHECK[@]}"; do
  include=${INCLUDE_CHECK[$i]}
  package=${INCLUDE_PACKAGES[$i]}
  if find ${include} 2>/dev/null >/dev/null; then
    echo "${package} already installed"
  else
    PACKAGES+=(${package})
  fi
done

if [ ${#PACKAGES[@]} -ne 0 ]; then
  echo
  echo "Please run the following command as root to install the required packages and re-run the install script."
  echo

  echo yum install -y ${PACKAGES[@]}

  exit 1
fi

# Build Ruby
./scripts/build/ruby.sh

# Build Node
./scripts/build/node.sh

# Build npm
./scripts/build/npm.sh

# Setup from here
./scripts/install/common.sh
