# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2019 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "../people"

# @api api
# @!group People API
class Occam
  module Controllers
    # These routes handle rendering and updating the profile page of a Person.
    #
    # This includes metadata and avatar images that represent this Person.
    class PersonProfileController < Occam::Controllers::PersonController
      # Renders the bookmark listing
      get '/people/:identity(/profile)?/?' do
        renderPersonView("profile")
      end

      # Form to edit an person's profile
      get '/people/:identity(/profile)?/edit' do
        person = Occam::Person.new(:identity => params[:identity],
                                   :account  => current_account)

        if not person.exists?
          status 404
        else
          # TODO: allow person editing for administrators
          if current_person.id != person.id
            status 406
          else
            render :slim, :"people/edit", :layout => !request.xhr?, :locals => {
              :errors  => nil,
              :person => current_person
            }
          end
        end
      end

      # Update person information
      post '/people/:identity(/profile)?/?' do
        person = Occam::Person.new(:identity => params[:identity],
                                   :account  => current_account)

        if not person.exists?
          status 404
          return
        end

        if not logged_in?
          status 404
          return
        end

        # Do not authorize this edit if they aren't logged in
        # TODO: allow person editing for administrators
        if person.id != current_person.id
          status 406
          return
        end

        data = []

        ["organization", "name", "description", "email"].each do |key|
          data << [key, params[key]]
        end

        person = person.set(data)
        if person.nil?
          status 422
          return
        end

        # Update avatar image if it is given
        if params["avatar"]
          if params["avatar"][:tempfile]
            person = person.set(params["avatar"][:tempfile].read, "avatar.jpg")
            person = person.set([ ["images", "[\"avatar.jpg\"]"] ], "object.json", :type => "json")
          end
        end

        redirect person.url
      end

      # Retrieves the avatar image for the particular person
      get '/people/:identity/avatar' do
        person = Occam::Person.new(:identity => params[:identity],
                                   :account  => current_account)

        if not person.exists?
          status 404
        else
          url = person.avatar_url((params["size"] || 64).to_i)

          if url.start_with?("data")
            content_type "image/png"
            person.avatar
          else
            redirect url
          end
        end
      end

      # Retrieves the avatar image for the particular person
      get '/people/:identity/:revision/avatar' do
        person = Occam::Person.new(:identity => params[:identity],
                                   :revision => params[:revision],
                                   :account  => current_account)

        if not person.exists?
          status 404
        else
          url = person.avatar_url((params["size"] || 64).to_i)

          if url.start_with?("data")
            content_type "image/png"
            person.avatar
          else
            redirect url
          end
        end
      end
    end
  end

  use Controllers::PersonProfileController
end
