# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2019 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group Theme API
class Occam
  module Controllers
    class ThemeController < Occam::Controller
      # Grab the themed version of the stylesheet
      get "/stylesheets/:theme/theme.css" do
        # Get the theme information
        begin
          theme = Occam::System::Theme.new(params[:theme])
        rescue
          status 404
          return nil
        end

        # Generate the theme file (if it exists, regenerate if theme is newer)
        # TODO: look at mtime
        themePath = File.join("stylesheets", "themes")
        if not File.exist?(themePath)
          Dir.mkdir(themePath)
        end

        themePath = File.join("stylesheets", "themes", params[:theme])
        if not File.exist?(themePath)
          Dir.mkdir(themePath)
        end

        themeSCSSPath = File.join(themePath, "theme.scss")

        #if not File.exist?(themeSCSSPath)
        File.open(themeSCSSPath, "w+") do |f|
          f.write(theme.scss)
        end
        #end

        content_type "text/css"
        render :scss, :"../stylesheets/themes/#{params[:theme]}/theme"
      end
    end
  end

  use Controllers::ThemeController
end
# @!endgroup
