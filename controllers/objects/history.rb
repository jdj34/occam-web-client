# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2019 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "../objects"

# @api api
# @!group Object API
class Occam
  module Controllers
    # This controller handles all history requests for an object.
    class ObjectHistoryController < Occam::Controllers::ObjectController
      # Retrieves the History tab or related metadata.
      #
      # Returns a 404 if the object is not found.
      #
      # @!macro object_route
      # @param [String] extension The extension to force the content to render as.
      #                           This can also be set using the Accept HTTP header.
      #
      # === Accepted Content Types:
      #
      # text/html (_default_), application/json
      #
      # === Examples:
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/history
      #
      # Retrieves an HTML document that renders the history of the object.
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/history.json
      #
      # Retrieves a JSON document that describes the history of the object. Returns
      # an array of revision metadata. That metadata will have, at least, an 'revision' and
      # likely also include a 'message' which describes the revision and an 'author'.
      # It may also include a 'date' which contains a iso8601 parsable date string.
      get %r{#{OBJECT_ROUTE_REGEX}/history(?<extension>\.json)?/?} do
        @object = resolveObject()
        format = request.preferred_type(['text/html', 'application/json'])

        if params[:extension] == ".json"
          format = "application/json"
        end

        case format
        when 'application/json'
          content_type 'application/json'
          @object.retrieveHistory(:before => params[:before],
                                  :after  => params[:after]).to_json
        else
          renderObjectView("history", {
            :before => params[:before],
            :after  => params[:after]
          })
        end
      end
    end
  end

  use Controllers::ObjectHistoryController
end
