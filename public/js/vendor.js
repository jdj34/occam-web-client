import elementQSAScope from "element-qsa-scope";

import elementResizeEvent from "element-resize-event";

window.elementResizeEvent = elementResizeEvent;

import { Terminal as XTerm } from "xterm";
import * as fit from 'xterm/lib/addons/fit/fit';

window.XTerm = XTerm;
window.XTerm.applyAddon(fit);

import moment from "moment";

window.moment = moment;

import Tagify from "@yaireo/tagify";

window.Tagify = Tagify;

window.d3 = {}

import { select, selectAll } from "d3-selection";

window.d3.select = select;
window.d3.selectAll = selectAll;

import { queue } from "d3-queue";

window.d3.queue = queue;

import { timer } from "d3-timer";

window.d3.timer = timer;

import { json } from "d3-request";

window.d3.json = json;

//import * as d3 from "d3";
//window.d3 = d3;

import { geoPath, geoOrthographic, geoGraticule, geoDistance } from "d3-geo";

window.d3.geoPath = geoPath;
window.d3.geoOrthographic = geoOrthographic;
window.d3.geoGraticule = geoGraticule;
window.d3.geoDistance = geoDistance;

import * as topojson from "topojson-client";

window.topojson = topojson;
