"use strict";

import EventComponent from './event_component.js';
import Util           from './util.js';
import Occam          from './occam.js';

/**
 * This class represents an object list, which is used for displaying the list of dependencies or resources on the details page.
 */
class ObjectList extends EventComponent {
    constructor(element) {
        super();

        if (element.getAttribute('data-object-list-index') == null) {
            ObjectList._count++;

            element.setAttribute('data-object-list-index', ObjectList._count);
            ObjectList._loaded[element.getAttribute('data-object-list-index')] = this;
        }

        this.element = element;

        // Get the object template
        this.template = element.querySelector("template");

        // Bind events
        this.bindEvents();
    }

    /**
     * Returns an instance of ObjectList for the given element.
     *
     * This will create an ObjectList, if it doesn't exist, for this element.
     *
     * @param {HTMLElement} element The main element for the object list;
     *                              typically the `<ul>` element.
     */
    static load(element) {
        if (element === undefined) {
            return null;
        }

        var index = element;
        if (typeof element != "number") {
            index = element.getAttribute('data-object-list-index');
        }

        if (index) {
            return ObjectList._loaded[index];
        }

        return new ObjectList(element);
    }

    /**
     * Loads all object lists found within the given element.
     */
    static loadAll(element) {
        if (!element) {
            throw new TypeError("element argument required");
        }

        if (!element.tagName) {
            throw new TypeError("element must be an HTMLElement");
        }

        var lists = element.querySelectorAll('.objects-container ul.objects');

        lists.forEach( (element) => {
            ObjectList.load(element);
        });
    }

    /**
     * Attaches events to the element.
     */
    bindEvents() {
        this.element.querySelectorAll(":scope > li.object:not(.empty)").forEach( (subElement) => {
            this.bindItemEvents(subElement);
        });
    }

    /**
     * Attaches events to a list item.
     */
    bindItemEvents(element) {
        // Bind asynchronous deletion button
        let deleteButton = element.querySelector("button.remove");
        if (deleteButton) {
            deleteButton.addEventListener("click", (event) => {
                event.stopPropagation();
                event.preventDefault();

                this.remove(element);
            });
        }
    }

    /**
     * Updates the item element to reflect the given information.
     */
    update(entry, options) {
        ["id", "uid", "revision", "name", "relation", "source", "type"].forEach( (key) => {
            if (options[key]) {
                let subElement = entry.querySelector('p.' + key);
                if (subElement) {
                    subElement.textContent = options[key].trim();
                }
            }
        });

        if (!options.copy) {
            let copyElement = entry.querySelector("a.copy");
            if (copyElement) {
                copyElement.remove();
            }
        }

        if (!options.download) {
            let downloadElement = entry.querySelector("a.download");
            if (downloadElement) {
                downloadElement.remove();
            }
        }

        if (true || options.loading) {
            let iconElement = entry.querySelector("a:first-child > img.icon");
            if (iconElement) {
                iconElement.src = "/images/indicators/object.svg";
            }

            entry.classList.add("loading");
        }
    }

    /**
     * Removes all entries in the list.
     */
    clear() {
    }

    /**
     * Appends the given item to the object list at the given location.
     *
     * @param {Object} options - The information to use to describe the new object.
     * @param {number} atIndex - The index to position the new element. Defaults to -1, which appends to the end of the list.
     */
    append(options, atIndex = -1) {
        if (!this.template) {
            throw new Error("Cannot add a new element to this object list: no template to use.");
        }

        // Clone a node
        let dummy = this.template;
        let newElement = null;
        if ('content' in dummy) {
            newElement = document.importNode(dummy.content, true);
            newElement = newElement.querySelector("li.object");
        }
        else {
            newElement = dummy.querySelector("li.object").cloneNode(true);
        }

        if (atIndex < 0) {
            atIndex = this.length;
        }

        if (atIndex >= 0) {
            atIndex = Math.min(atIndex, this.length);
        }

        let anchor = this.elementFor(atIndex);
        if (!anchor) {
            anchor = this.element.querySelector(":scope > li.object.empty");
        }
        console.log(anchor);

        if (!anchor) {
            // Append to the list.
            this.element.appendChild(newElement);
        }
        else {
            this.element.insertBefore(newElement, anchor);
        }

        // Bind item events
        this.bindItemEvents(newElement);

        // Bind events.
        Occam.loadAll(newElement);

        // And fill in element with the given information.
        this.update(newElement, options);

        return newElement;
    }

    /** Removes the list item by either index or element.
     */
    remove(index_or_element) {
        let subElement = this.elementFor(index_or_element);

        // Trigger remove form, if it exists (and this isn't still loading)
        let deleteButton = subElement.querySelector("button.remove");
        if (deleteButton && !subElement.classList.contains("loading")) {
            let form = Util.getParents(deleteButton, "form", "form")[0];
            if (form) {
                subElement.classList.add("removing");
                Util.submitForm(form, () => {
                    // Remove item from document
                    subElement.remove();
                });
            }
        }
    }

    /**
     * Returns the number of items currently in the list.
     */
    get length() {
        return this.element.querySelectorAll(":scope > li.object:not(.empty)").length;
    }

    /**
     * Replaces a list item with the given element.
     */
    replace(index_or_element, newEntry) {
        var oldEntry = this.elementFor(index_or_element);
        this.element.replaceChild(newEntry, oldEntry);

        this.bindItemEvents(newEntry);
        Occam.loadAll(newEntry);

        return newEntry;
    }

    /**
     * Retrieves the list element for the given index.
     *
     * Returns null when the item cannot be found.
     */
    elementFor(index_or_element) {
        if (typeof index_or_element != "number" && typeof index_or_element != "string") {
            return index_or_element;
        }

        return this.element.querySelector(":scope > li.object:nth-of-type(" + (index_or_element + 1) + ")");
    }

    /**
     * Retrieves the information represented by the given index.
     */
    infoFor(index_or_element) {
        var entry = this.elementFor(index_or_element);

        let ret = {};

        ["id", "uid", "revision", "name", "relation", "source", "type"].forEach( (key) => {
            let subElement = entry.querySelector('p.' + key);
            if (subElement) {
                ret[key] = subElement.textContent.trim();
            }
        });

        return ret;
    }
}

ObjectList._count = 0;
ObjectList._loaded = {};

export default ObjectList;
