"use strict";

/**
 * Represents the earth for the purposes of visualizing object location.
 *
 * The earth is a sphere. I am officially part of the globehead conspiracy.
 */
export class Globe {
    constructor(element, options) {
        let d3 = window.d3;

        this._element = element;
        this._options = Object.assign({}, Globe.defaultOptions, options);

        this._svg = d3.select('svg');

        let width = this._element.querySelector("svg").clientWidth;
        let height = width;

        this._initialWidth = width;
        this._initialHeight = height;

        width = this._element.clientWidth;
        height = width;

        // Initialize markers
        this._markerGroup = this._svg.append('g');
        this._locations = [];

        this._projection = d3.geoOrthographic();
        this._initialScale = this._projection.scale();
        this._path = d3.geoPath().projection(this._projection);

        window.addEventListener("resize", (event) => {
            this._svg.attr('width', this._initialWidth)
                     .attr('height', this._initialHeight);

            let width = this._element.clientWidth;
            let height = width;

            this._resize(width, height);
        });

        this._resize(width, height);

        if (this._options.graticule) {
            this._initializeGraticule();
        }

        this._initializeGlobe();

        this.rotate = this._options.rotate;

        this.addPoint(40.4406, -79.9959);
    }

    /**
     * Updates the projection to fit the given space.
     */
    _resize(width, height) {
        this._center = [width / 2, height / 2];
        this._projection.translate(this._center)
        this._svg.attr('width', width)
                 .attr('height', height);
        this._projection.scale(width/this._initialWidth * 249.5);
    }

    /**
     * Initializes the globe structure and data.
     */
    _initializeGlobe() {
        let d3 = window.d3;

        d3.queue()
          .defer(d3.json, '/data/land-110m.json')
          .await( (error, worldData) => {
              this._feature = window.topojson.feature(worldData, worldData.objects.land).features;
              this._svg.selectAll(".segment")
                       .data(this._feature)
                       .enter().append("path")
                               .attr("class", "segment")
                               .attr("d", this._path)
                               .style("stroke-width", "1px");

              let angle = this._options.angle;
              let verticalTilt = this._options.verticalTilt;
              let horizontalTilt = this._options.horizontalTilt;
              this._projection.rotate([angle, verticalTilt, horizontalTilt]);
              this._svg.selectAll("path").attr("d", this._path);
              this._loaded = true;

              if (this._options.rotate) {
                  this._rotate();
              }
          });
    }

    /**
     * Initializes the graticule (grid lines) around the globe.
     */
    _initializeGraticule() {
        let d3 = window.d3;

        this._graticule = d3.geoGraticule().step([10, 10]);

        if (!this._element.querySelector("path.graticule")) {
            this._svg.append("path")
                     .attr("class", "graticule")
                     .datum(this._graticule)
                     .attr("d", this._path)
                     .style("fill", "transparent");
        }
        else {
            this._svg.selectAll(".graticule")
                     .datum(this._graticule)
                     .attr("d", this._path)
                     .style("fill", "transparent");

            let angle = this._options.angle;
            let verticalTilt = this._options.verticalTilt;
            let horizontalTilt = this._options.horizontalTilt;
            this._projection.rotate([angle, verticalTilt, horizontalTilt]);
            this._svg.selectAll("path").attr("d", this._path);
        }
    }
    
    /**
     * Redraws the globe.
     */
    drawGlobe() {
    }
    
    /**
     * Internal function that will draw the graticule.
     *
     * A graticule is the projected lines around the sphere.
     */
    _drawGraticule() {
    }

    /**
     * Toggles rotation.
     */
    set rotate(value) {
        this._options.rotate = value;

        if (this._options.rotate && this._loaded) {
            this._rotate();
        }
    }

    /**
     * Enables a timer to rotate the globe.
     */
    _rotate() {
        let d3 = window.d3;

        d3.timer( (elapsed) => {
            let angle = this._options.angle + (this._options.speed * elapsed);
            let verticalTilt = this._options.verticalTilt;
            let horizontalTilt = this._options.horizontalTilt;
            this._projection.rotate([angle, verticalTilt, horizontalTilt]);
            this._svg.selectAll("path").attr("d", this._path);
            this._drawMarkers();
        });
    }

    /**
     * Adds a marker to the globe.
     *
     * @param {number} latitude - The latitude of the coordinate.
     * @param {number} longitude - The longitude of the coordinate.
     */
    addPoint(latitude, longitude) {
        this._locations.push({
            longitude: longitude,
            latitude: latitude
        });

        this._createdMarkers = false;
        this._drawMarkers();
    }

    _drawMarkers() {
        let d3 = window.d3;

        if (!this._createdMarkers) {
            // Append the points to the globe
            const markers = this._markerGroup.selectAll('circle')
                                             .data(this._locations);

            let group = markers.enter()
                               .append('g');

            group.append('circle')
                 .merge(markers)
                 .attr("class", "marker-aura")
                 .attr('r', 3);

            group.append('circle')
                 .merge(markers)
                 .attr("class", "marker")
                 .attr('r', 3);

            this._markerGroup.each(function () {
                this.parentNode.appendChild(this);
            });

            this._createdMarkers = true;
        }

        // Just move them
        this._markerGroup.selectAll('circle.marker, circle.marker-aura')
            .attr('cx', d => this._projection([d.longitude, d.latitude])[0])
            .attr('cy', d => this._projection([d.longitude, d.latitude])[1])
            .attr('opacity', d => {
                 const coordinate = [d.longitude, d.latitude];
                 let gdistance = d3.geoDistance(coordinate, this._projection.invert(this._center));
                 return gdistance > 1.57 ? '0' : '1';
            })
    }
}

Globe.defaultOptions = {
    speed: 0.005,
    verticalTilt: -30,
    horizontalTilt: 0,
    rotate: false,
    graticule: true,
    angle: 0
};

export default Globe;
