"use strict";

import EventComponent from "../event_component.js";
import Search         from '../search.js';
import OccamObject    from '../occam_object.js';
import Util           from "../util.js";
import Tabs           from "../tabs.js";
import Modal          from "../modal.js";

/**
 * This implements behaviors for the objects/new modal.
 */
class AddDependencyModal extends EventComponent {
    constructor(element) {
        super();

        if (element === undefined) {
            return;
        }

        this.element = element;

        // Find the errors section
        this.errorsList = this.element.parentNode.querySelector(".errors");

        // Find the form
        this.form = this.element.parentNode.querySelector("form#new-dependency");

        // Find the button and other form elements
        if (this.form) {
            this.messageField = this.form.querySelector('input[name="message"]');
            this.messageTemplate = this.messageField.value;
            this.nameField = this.form.querySelector('input#add-dependency-name');
            this.typeField = this.form.querySelector('input#add-dependency-type');
            this.idField = this.form.querySelector('input#add-dependency-id');
            this.uidField = this.form.querySelector('input#add-dependency-uid');
            this.submitButton = this.form.querySelector('input[name="submit"]');
            this.versionField = this.form.querySelector('input#add-dependency-version');
            this.summaryField = this.form.querySelector('input#add-dependency-summary');

            this.previewObject = this.form.querySelector(".objects-container ul.objects li.object"); 
            this.previewLink = this.form.querySelector(".objects-container ul.objects li.object a"); 
            this.previewImage = this.previewObject.querySelector("img.icon");
            this.previewName = this.previewObject.querySelector("p.name");
            this.previewType = this.previewObject.querySelector("p.type");
        }

        // Tabs
        this.tabs = Tabs.load(this.element.querySelector("ul.tab-bar"));

        // Search
        this.search = Search.load(this.element);

        AddDependencyModal._count++;
        this.element.setAttribute('data-add-dependency-modal-index', AddDependencyModal._count);

        AddDependencyModal._loaded[this.element.getAttribute('data-add-dependency-modal-index')] = this;

        // Detect if part of Modal (I know... confusing, but this can be not-a-modal)
        this.withinModal = false;
        if (Util.getParents(this.element, ".modal-window", ".modal-window").length > 0) {
            this.withinModal = true;
        }

        this.bindEvents();
    }

    static loadAll(element) {
        var elements = element.querySelectorAll('h1.new-dependency ~ .card.new-dependency');

        elements.forEach(function(element) {
            AddDependencyModal.load(element);
        });
    }

    static load(element) {
        if (element === undefined) {
            return null;
        }

        var index = element.getAttribute('data-add-dependency-modal-index');

        if (index) {
            return AddDependencyModal._loaded[index];
        }

        return new AddDependencyModal(element);
    }

    bindEvents() {
        if (this.tabs) {
            this.tabs.on('change', () => {
                // TODO: handle sidebar changes, if needed
            });
        }

        // Handle selection of object
        if (this.search) {
            this.search.on('change', () => {
                // Search results were updated...
                // We can bind events on the object listing.
                let list = this.search.element.querySelector(".results .search-results-preview-container");

                list.querySelectorAll("li.object a").forEach( (object) => {
                    object.addEventListener('click', (event) => {
                        event.stopPropagation();
                        event.preventDefault();

                        let node = object.parentNode;

                        if (this.form) {
                            // Update form entries
                            this.nameField.value = node.querySelector('p.name').textContent.trim();
                            this.typeField.value = node.getAttribute('data-object-type');
                            this.idField.value = node.getAttribute('data-object-id');
                            this.uidField.value = node.getAttribute('data-object-uid');
                            this.submitButton.removeAttribute('disabled');

                            let summaryValue = node.querySelector('p.summary');
                            if (summaryValue) {
                                this.summaryField.value = summaryValue.textContent;
                            }

                            this.versionField.value = "";
                            this.versionField.classList.add("loading");

                            // Update dummy object representation
                            this.previewImage.src = node.querySelector("img.icon").src;
                            this.previewName.textContent = this.nameField.value;
                            this.previewType.textContent = this.typeField.value;
                            this.previewObject.classList.remove("empty");
                            this.previewLink.classList.remove("empty");
                            //this.previewLink.removeAttribute('disabled');

                            // Pull down version information
                            let object = new OccamObject(this.idField.value);
                            let statusURL = object.url({
                                path: "/status"
                            });

                            Util.getJSON(statusURL, (data) => {
                                // Pull out a version number
                                if (data.sortedVersions) {
                                    var latest = data.sortedVersions[data.sortedVersions.length - 1];
                                    this.versionField.value = "~" + latest;
                                }

                                this.versionField.classList.remove("loading");
                            });

                            // Switch over to the metadata tab
                            this.tabs.select(1);

                            // Update commit message text
                            this.messageField.value = Util.formatString(this.messageTemplate, {
                                name: this.nameField.value,
                                type: this.typeField.value
                            });

                            // Submit the form and close the modal, if within one.
                            if (this.withinModal) {
                                /*Util.submitForm(this.form, (html) => {
                                    Modal.close();
                                });*/
                            }
                        }
                    });
                });
            });
        }
    }
}

AddDependencyModal._count  = 0;
AddDependencyModal._loaded = {};

export default AddDependencyModal;
