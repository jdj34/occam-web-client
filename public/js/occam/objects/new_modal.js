"use strict";

import EventComponent from "../event_component.js";
import Selector       from "../selector.js";
import AutoComplete   from "../auto_complete.js";
import Util           from "../util.js";

/**
 * This implements behaviors for the objects/new modal.
 */
class NewModal extends EventComponent {
    constructor(element) {
        super();

        if (element === undefined) {
            return;
        }

        this.element = element;

        // Find the errors section
        this.errorsList = this.element.parentNode.querySelector(".errors");

        // Find the text input for the object type
        this.typeInput = this.element.querySelector('input[name="type"]');
        this.typeAutoComplete = AutoComplete.load(this.typeInput);

        // Find the template input for the object type
        this.templateInput = this.element.querySelector('input[name="template"]');
        this.templateAutoComplete = AutoComplete.load(this.templateInput);

        // Find the button
        this.submitButton = this.element.querySelector('input[name="add"]');

        NewModal._count++;
        this.element.setAttribute('data-new-modal-index', NewModal._count);
        NewModal._loaded[this.element.getAttribute('data-new-modal-index')] = this;

        this.bindEvents();
    }

    static loadAll(element) {
        var elements = element.querySelectorAll('h1.create ~ * form.modal.new-object');

        elements.forEach(function(element) {
            NewModal.load(element);
        });
    }

    static load(element) {
        if (element === undefined) {
            return null;
        }

        var index = element.getAttribute('data-new-modal-index');

        if (index) {
            return NewModal._loaded[index];
        }

        return new NewModal(element);
    }

    bindEvents() {
        if (this.templateAutoComplete && this.typeInput) {
            this.templateAutoComplete.on("change", () => {
                // Update the type field to the type of the template
                this.typeInput.value = this.templateAutoComplete.type();
            });
        }
    }
}

NewModal._count  = 0;
NewModal._loaded = {};

export default NewModal;
