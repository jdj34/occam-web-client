require 'sinatra'

class Occam < Sinatra::Base
  # Load configurations for plugins, etc
  require_relative '../lib/config'
end
