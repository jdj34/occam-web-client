# Getting Started

![width=40rem|padding=3rem|!The Occam Logo which depicts a simple purple cube with a set of rulers measuring it.](/images/logo_squish_fitted.svg)

If this is your first time here, welcome to Occam!

In this guide, you will learn the basics of creating new things in Occam.

## Installation

This guide assumes you are learning how to use Occam.
If you would like to install Occam yourself, then go to the [installation guide](/documentation/installation) instead.

## Basics

Occam is an archival tool focusing on the repeatability of software. That is, being able to build and run software
once and then, in the future, run it again as close to the same way as possible.
It does this by keeping track of software and its dependencies and storing them in a highly available way.

This site you are currently on serves as a portal to creating and using software that has been made using the Occam
platform. You, too, will be able to create or wrap existing software using Occam and share that for others to use.

This guide serves as a basic tutorial for creating a script in Occam, which is only one small way that Occam can be
used. It will step through the process in a very matter-of-fact way, but you are free to explore on your own.
Throughout the site, you will see ![inline|!help](/images/dynamic/hex/ae85db/icons/ui/info.small.svg) icons that will pop up
a helpful message about that area of the site when you invoke them.

## Creating an Account

The first thing you will want to do is sign up for your own account.
You can do many things as a guest (without logging on), but you will be unable to create and save new objects.

To create a new account, click %% navigation | signup %% and fill in the fields with your desired username and password.
These will be used each time you wish to log on to this particular Occam server and revisit your work.

You will be logged in and presented with your dashboard page. Navigate to the %% tab | people | collection %% page to see
a list of your ongoing work. Naturally, this will be empty for a new account. So we will create a simple script to start with.

## Creating a New Script

First, we will create a small script. Don't worry, it won't require much programming.
We will use a template so that we get some of the more repetitive basic steps out of the way more quickly.

At the top of the collection page, use the %% button | people | collection | bar | new %% button to bring up a dialog to create
a new item in your collection.

Here, we could create the default item, which is an empty object or experiment (workflow), however we will start with a Python
script using a template. Click on the template field and start typing "Python" and select Python Script from the dropdown.

Once you have that, it will automatically fill out the object type as "script" and you will then be able to enter in a name for
your script. You can be imaginative, or just type "My Python Script" here.

Once you are done, press the %% button | people | collection | new | submit %% button and it will navigate to your new script.

## Running Your New Script

The initial template will have a simple Python script already provided that will do something simple.
To test everything and become more familiar with the interface, let's just run that simple script as it is.

Navigate to the %% tab | objects | run %% page using the tab bar to see the options we have to execute our script.
We will simply press %% button | objects | run | form | run | button %%.

This will create a manifest for running your script, generate a job that will be queued, and then eventually invoke your script
and show a live view of the running task. When it is done, the output should perform the simple script and show you:

```
Hello
```

Now, this is not very interesting, of course. So, let's modify the file.

## Modifying Your Script.

Go to the %% tab | objects | files %% page and you will see the list of files
that comprise your object. For our template, this is just a single `main.py` script. You can open a file by navigating to that
file in the listing and clicking on it. This will open the file in a viewer and show you the following text:

```python
print("Hello")
```

So, let's add some code and see what happens. Let's update the file to the following (you can copy and paste from the section
below:)

```python
import numpy as np
a = np.arange(15).reshape(3, 5)
print(a)
```

This will use [modal|data-modal-large|numpy](/QmX8fe1jBgfp6m4WrLFxM2ppKbVG8zNJTVfxS4maUQq4DB/5dsSuentxGZmSYXXWwXwFw45XgjaTp) to create a matrix and then
print that matrix to the screen. The dimensions of the matrix are 3 by 5 and the values are supplied as an integer range from 1 to 15.

**Make sure you save the file.** Afterward, go back to the %% tab | objects | run %% tab and press the %% button | objects | run | form | run | button %% button again.

This time, you will see the following output:

```
Traceback (most recent call last):
  File "/occam/QmeLUK8o7nXTa5A7JkR4odjBjLf8pu3hjQvjS7wephBPcE-5dtJT2WQVh6Jyx3UscpqQyeeBkAxms/main.py", line 2, in <module>
    import numpy as np
ModuleNotFoundError: No module named 'numpy'
```

This means that numpy was not specified as a dependency to our script. By default, not every python library in the world is
automatically available to your script. You may have a need for a specific version of a specific library, after all.

To faciliate this, we have to specify that we need numpy.

## Adding Dependencies

Navigate to the %% tab | objects | metadata %% page to see the metadata for your script.
There will be a section called %% header | objects | metadata | dependencies %% which lists each object that must be included
when our script is invoked.
There is a button at the end of the list that will allow you to append a new dependency.
Invoking this button will bring up a dialog that will allow you to search for and then add such an object.

In the query field of the dialog, type `numpy`. At least two objects should appear of the type `python-library`.
`numpy-2` is for Python 2.x, which is an older version of Python than we are using. We want the `numpy` object.
*Generally, you will want to include the python libraries without the '-2' suffix.*

Click on `numpy` and then click the %% button | objects | metadata | new-dependency | submit %% button. It should show you a new object in the list with the name "numpy."

Now simply navigate once more to the %% tab | objects | run %% page and press that %% button | objects | run | form | run | button %% button a final time.
You will, if all goes well, see the following:

```
[[ 0  1  2  3  4]
 [ 5  6  7  8  9]
 [10 11 12 13 14]]
```

With this output, you now have a starting point for writing scientific Python. Enjoy!
