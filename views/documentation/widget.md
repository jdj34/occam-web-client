# Widgets

In order to best accommodate the ever-growing list of data formats, viewers, and editors, Occam allows for interactive widgets to be developed that can directly interact with that data within the browser.
This is done, currently, through a JavaScript API that any developer can target.
Throughout this document, the motivations, use, and a tutorial will guide you through developing your own widget.

## Motivation

You can wrap and then use existing JavaScript widgets as they are as just a simple HTML page.
However, there are a few reasons to go through the small effort of adding the appropriate API calls.
For instance,

* Gives people a way to play with and explore data more thoroughly.
* Helps visualize complicated and possibly bespoke data.
* Integrates with the existing user-interface.

## Showcase

There are two different categories of widgets: viewers and editors.
When you view an Object or open a file within that Object, it will use the viewer you have associated with that type of Object.
Here are a few examples showcasing the different aspects of widgets.

![border|!](/images/index/screens/DRAMSim2_Editing_Ace.png)

## API

**To Be Described**: API between widget and web client.

```javascript
window.postMessage(message, '*');
```

```javascript
window.addEventListener('message', (event) => {
  switch(event.data.name) {
    case 'updateStatus': 
      break;
  }
});
```

## Tutorial

**To Be Described**: A tutorial and template to make widgets and visualizations.

<!--

Let's build a simple widget that opens a particular file.

First, go to your collection and create a new Object using the New button in the top-left corner of the toolbar.

To start, we will use an Object template which gives us nice boilerplate to create our new widget.
In the field marked "Template", type Widget until it gives you the appropriate choice. -->
