This directory houses all of the documents that comprise the Occam user documentation.

To add a documentation page, just write a markdown file with a `md` extension and it will appear on the `/documentation` page.

The markdown should contain a header (a single '#' before the text on a line) to denote its title. This header will be hidden when rendered, but the name will be used to name the page on the tab list.

Any subheader (a double '#' before the text on a line) will populate the left-hand navigation list.
Use these to separate different sections of the overall page.

The documentation page will be rendered by searching for every `.md` file and creating a tab for each one as they appear in alphabetical order based on their filenames.
To force a paricular page order, just add numbers before the filename when appropriate.

Changing the name of the file will not affect the URL of the document.
That is defined by the header within the markdown.
Updating the header, however, will change the URL, so care must be taken to try to preserve the URLs for older pages by adding redirects to the DocumentationController.
