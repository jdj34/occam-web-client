In order to faciliate the proper composition of different software tools, Occam provides a graphical editor to stitch together a workflow.
You simply connect each artifact together in this editor and the system can execute the overall apparatus automatically.
Also provided is a configuration editor which provides a consistent way of updating and varying parameters for each node.
This makes it easy to explore a variety of outcomes by providing a value range in any of these configuration options, which will generate a run for each permutation.
