class Occam
  # This represents a set of tests within a test suite.
  class TestGroup
    attr_reader :tests

    # The different name fields based on the group type
    attr_reader :name
    attr_reader :key
    attr_reader :class
    attr_reader :method
    attr_reader :function
    attr_reader :feature

    attr_reader :describes
    attr_reader :features

    # Instantiates a TestGroup based on the given group information.
    #
    # Typically, the options passed to this will come from the JSON test
    # results data.
    #
    # @param [Hash] options The information that makes up this group.
    # @option options [String] :name The name of the group.
    # @option options [String] :key The simple key for this group.
    # @option options [String] :method The method name this group represents.
    # @option options [String] :function The function name this group
    #                                    represents.
    # @option options [String] :class The class name this group represents.
    # @option options [String] :type The type of group this group represents.
    # @option options [String] :describes A hash representing sub-groups within
    #                                     this group that are BDD 'describes'.
    # @option options [String] :features A hash representing sub-groups within
    #                                    this group that are 'features'.
    # @option options [String] :tests A hash representing the tests within
    #                                 this group. See the Test class.
    def initialize(options)
      @name     = options[:name]
      @key      = options[:key]
      @method   = options[:method]
      @function = options[:function]
      @feature  = options[:feature]
      @class    = options[:class]
      @type     = options[:type]

      # Describe/It style group
      @describes = {}
      if options[:describes]
        options[:describes].each do |key, group|
          @describes[key] = Occam::TestGroup.new(group)
        end
      end

      # Feature/Scenario style group
      @features = {}
      if options[:features]
        options[:features].each do |key, group|
          @features[key] = Occam::TestGroup.new(group)
        end
      end

      @tests = (options[:tests] || []).map do |test|
        Occam::Test.new(test)
      end
    end

    # Returns the type of group this is.
    #
    # This depends on the presence of a method, function, class or feature
    # option upon creation. This can be overriden by specifying a :type
    # option upon creation.
    #
    # @return [Symbol] The group type. May be :method, :function, :class,
    #                  :feature, :group, or some custom type.
    def type
      if @type
        @type.intern
      elsif self.method
        :method
      elsif self.function
        :function
      elsif self.class
        :class
      elsif self.feature
        :feature
      else
        :group
      end
    end

    # Returns the name of what the group is testing.
    def header
      self.method || self.function || self.class || self.feature || self.name || self.key
    end

    # Returns the set of tests or sub-groups this group represents.
    def items
      self.tests || self.describes || self.features
    end

    # Returns the number of total tests within this group.
    #
    # This will recurse through sub-groups as well to determine a total number
    # of tests overall.
    #
    # @return [Integer] The total number of tests for this group and sub-groups.
    def total
      if !defined?(@total)
        total = (self.tests || []).length

        (self.describes || {}).each do |key, group|
          total += group.total
        end
        (self.features || {}).each do |key, group|
          total += group.total
        end

        @total = total
      end

      @total
    end

    # Returns the statistics data for all tests within this group.
    #
    # This will recurse through sub-groups as well to determine an aggregate
    # result for tests overall.
    #
    # The data should consist of one or more of the following statistics:
    # :total is the number of tests overall.
    # :assertions is the number of assertions.
    # :failures is the number of failed tests.
    # :errors is the number of errored tests.
    # :skips is the number of skipped tests.
    # :passes is the number of passing tests.
    #
    # @return [Hash] A data table for the statistics of this group and
    #                sub-groups.
    def statistics
      if !defined?(@statistics)
        @statistics = {
          :passes     => 0,
          :errors     => 0,
          :failures   => 0,
          :skips      => 0,
          :total      => self.total
        }

        (self.tests || []).each do |test|
          if test.type.intern == :passed
            @statistics[:passes]   += 1
          elsif test.type.intern == :skipped
            @statistics[:skips]    += 1
          elsif test.type.intern == :failed
            @statistics[:failures] += 1
          elsif test.type.intern == :error
            @statistics[:errors]   += 1
          end
        end

        (self.describes || {}).each do |key, group|
          @statistics[:passes]   += group.statistics[:passes]
          @statistics[:failures] += group.statistics[:failures]
          @statistics[:errors]   += group.statistics[:errors]
          @statistics[:skips]    += group.statistics[:skips]
        end

        (self.features || {}).each do |key, group|
          @statistics[:passes]   += group.statistics[:passes]
          @statistics[:failures] += group.statistics[:failures]
          @statistics[:errors]   += group.statistics[:errors]
          @statistics[:skips]    += group.statistics[:skips]
        end
      end

      @statistics
    end
  end
end
