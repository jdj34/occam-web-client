# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  # This class represents license metadata.
  #
  # This is something that is represented within the Occam::Object metadata.
  class License
    # The name of the license
    attr_reader :name

    # The canonical name of the license
    attr_reader :fullName

    # The license contents
    attr_reader :text

    # The filename for this license
    attr_reader :file

    # The full metadata block
    attr_reader :info

    # The index of the license within the object
    attr_reader :index

    def initialize(options)
      @info = options

      @name  = @info[:name]
      @text  = @info[:text]
      @file  = @info[:file]
      @index = @info[:index]

      @fullName = @info[:fullName] || @name

      @info.delete(:index)
    end

    # Retrieves the license information for the given license by ites key.
    def self.load(key, options={})
      arguments  = [key]
      cmdOptions = {}

      result = Occam::Worker.perform("licenses", "view", arguments, cmdOptions)
      JSON.parse(result[:data], :symbolize_names => true)
    end

    # Retrieves the license text for the given license by its key.
    def self.text(key, options={})
      arguments  = [key]
      cmdOptions = {"-k" => "text"}

      result = Occam::Worker.perform("licenses", "view", arguments, cmdOptions)
      JSON.parse(result[:data], :symbolize_names => true)
    end

    # Retrieves a list of all known license organizations.
    def self.organizations(options={})
      if !defined?(@@organizations)
        ret = []

        self.all.each do |license|
          if !ret.include?(license[:organization])
            ret << license[:organization]
          end
        end

        @@organizations = ret
      end

      @@organizations
    end

    # Retrieves a list of all known licenses.
    def self.all(options={})
      if !defined?(@@licenses)
        arguments  = []
        cmdOptions = {}

        result = Occam::Worker.perform("licenses", "list", arguments, cmdOptions)
        ret = JSON.parse(result[:data], :symbolize_names => true)

        # Sort by name
        ret = ret.sort do |a, b|
          a[:name] <=> b[:name]
        end

        @@licenses = ret
      end

      ret = @@licenses
      if options[:organization]
        ret = ret.filter do |item|
          item[:organization] == options[:organization]
        end
      end
      ret
    end
  end
end
