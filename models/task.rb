# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  class Task
    attr_reader :environment
    attr_reader :architecture
    attr_reader :targetEnvironment
    attr_reader :targetArchitecture
    attr_reader :object
    attr_reader :targetBackend
    attr_reader :phase

    def initialize(options)
      @object = options[:object]
      @account = options[:account] || (@object ? @object.account : nil)
      @token = options[:token] || (@object ? (@object.tokens || []).first : nil)

      if not @token
        guestToken = Occam::Config.configuration['guest']
        if guestToken
          @token = guestToken
        end
      end

      @targetBackend      = options[:targetBackend]
      @environment        = options[:environment]
      @architecture       = options[:architecture]
      @targetEnvironment  = options[:targetEnvironment]
      @targetArchitecture = options[:targetArchitecture]
      @inputs             = options[:inputs]
      @using              = options[:using]
      @phase              = options[:phase] || :run
    end

    def manifest
      if !defined?(@info)
        cmdOptions = {
        }

        if @token
          cmdOptions["-T"] = @token
        end

        if @account
          cmdOptions["-T"] = @account.token
        end

        if @targetBackend
          cmdOptions["--target-backend"] = @targetBackend
        end

        if @environment
          cmdOptions["--environment"] = @environment
        end

        if @architecture
          cmdOptions["--architecture"] = @architecture
        end

        if @targetEnvironment
          cmdOptions["--target-environment"] = @targetEnvironment
        end

        if @targetArchitecture
          cmdOptions["--target-architecture"] = @targetArchitecture
        end

        if @inputs
          @inputs.each do |input|
            cmdOptions["--input"] ||= []
            cmdOptions["--input"] << [input]
          end
        end

        if @using
          @using.each do |using|
            cmdOptions["--using"] ||= []
            cmdOptions["--using"] << [using]
          end
        end

        result = Occam::Worker.perform("manifests", @phase.to_s, [@object.fullID], cmdOptions)
        @info = JSON.parse(result[:data], :symbolize_names => true)
      end

      @info
    end

    def self.iconURLFor(backend, options = {})
      backend = backend.to_s.gsub("/", "-").gsub("+", "-")

      # Detect if the icon exists for this type
      basePath = "/images/dynamic"
      if options[:color]
        basePath = basePath + "/color/#{options[:color]}"
      elsif options[:hue]
        basePath = basePath + "/hue/#{options[:hue]}/sat/#{options[:sat]}/light/#{options[:light]}"
      elsif options[:hex]
        basePath = basePath + "/hex/#{options[:hex]}"
      else
        basePath = "/images"
      end

      publicPath = File.join("public", "images", "icons", "backends", backend)

      if options[:small] && File.exist?(publicPath + ".small.svg")
        "#{basePath}/icons/backends/#{backend}.small.svg"
      elsif options[:small] && File.exist?(publicPath + ".small.png")
        "#{basePath}/icons/backends/#{backend}.small.png"
      elsif File.exist?(publicPath + ".svg")
        "#{basePath}/icons/backends/#{backend}.svg"
      elsif File.exist?(publicPath + ".png")
        "#{basePath}/icons/backends/#{backend}.png"
      else
        options[:default] || "#{basePath}/icons/backends/any.svg"
      end
    end

    def route
      cmdOptions = {
        "--phase": @phase.to_s
      }

      if @targetEnvironment
        cmdOptions["--target-environment"] = @targetEnvironment
      end

      if @targetArchitecture
        cmdOptions["--target-architecture"] = @targetArchitecture
      end

      if @account
        cmdOptions["-T"] = @account.token
      end

      if @object
        result = Occam::Worker.perform("manifests", "route", [@object.fullID], cmdOptions)
        JSON.parse(result[:data], :symbolize_names => true).map do |object|
          if object.has_key? :id
            Occam::Object.new(:account => @account, :tokens => !@token.nil? ? [@token] : nil, :info => object, **object)
          else
            object
          end
        end
      else
        []
      end
    rescue
      []
    end
  end
end
