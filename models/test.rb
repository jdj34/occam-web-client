class Occam
  # Represents a single test within a larger test suite.
  class Test
    attr_reader :name
    attr_reader :scenario
    attr_reader :it
    attr_reader :class
    attr_reader :type
    attr_reader :time
    attr_reader :assertions

    # Instantiates a Test based on the given test information.
    #
    # @param [Hash] options The information that makes up this test.
    # @option options [String] :name The name of the test.
    # @option options [String] :it The 'it' text for a behavioral test.
    # @option options [String] :scenario The 'scenario' text for a feature test.
    # @option options [String] :class The class name this test.
    # @option options [String] :type The type of test.
    # @option options [String] :time The time, in seconds, this test took to
    #                                run.
    # @option options [String] :assertions The number of assertions within this
    #                                      test.
    def initialize(options)
      @name       = options[:name]
      @it         = options[:it]
      @scenario   = options[:scenario]
      @class      = options[:class]
      @type       = options[:type]
      @time       = options[:time]
      @assertions = options[:assertions]
    end

    # Returns the header text for this test.
    #
    # @return [String] The header text containing some name for this test.
    def header
      self.name || self.scenario || self.it
    end
  end
end
