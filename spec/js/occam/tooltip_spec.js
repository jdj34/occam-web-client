"use strict";

import Helper from '../helper';

import Tooltip from '../../../public/js/occam/tooltip';

describe('Tooltip', () => {
    beforeEach(function() {
        jasmine.addMatchers(DOMCustomMatchers);
        jasmine.addMatchers(Helper.VisibilityMatchers);

        // Create something with a title attribute
        let element = document.createElement("a");
        element.textContent = "link";
        let text = Helper.randomString();
        element.setAttribute('title', text);

        let content = document.createElement("div");
        content.classList.add("content");
        document.body.appendChild(content);
        content.appendChild(element);

        this.element = element;
        this.text = text;
    });

    afterEach(function() {
        Tooltip.element = undefined;
    });

    describe('.load', () => {
        it('should throw TypeError when element is not passed', function() {
            expect( () => Tooltip.load() ).toThrowError(TypeError);
        });

        it('should throw TypeError when element is not an HTMLElement', function() {
            expect( () => Tooltip.load("a.tooltip") ).toThrowError(TypeError);
        });
    });

    describe('#show', () => {
        it('should apply the correct text to the tooltip element', function() {
            let tooltip = Tooltip.load(this.element);
            tooltip.show();
            expect(Tooltip.element).toContainText(this.text);
        });

        it('should apply the text within the given attribute to the tooltip element', function() {
            let tooltip = Tooltip.load(this.element);
            let attribute = 'data-' + Helper.randomString();
            let text = Helper.randomString();
            this.element.setAttribute(attribute, text);
            tooltip.show(attribute);
            expect(Tooltip.element).toContainText(text);
        });

        it('should reveal the tooltip element', function() {
            let tooltip = Tooltip.load(this.element);
            tooltip.show();
            expect(Tooltip.element).toBeVisible();
        });
    });

    describe('#hide', () => {
        it('should hide the tooltip element', function() {
            let tooltip = Tooltip.load(this.element);
            tooltip.show();
            tooltip.hide();
            expect(Tooltip.element).toBeHidden();
        });
    });

    describe('Accessibility', () => {
        it('should hide tooltip when cursor moves away from tooltip', function() {
            let tooltip = Tooltip.load(this.element);
            tooltip.show();
            Helper.dispatchMouseEvent('mouseleave', Tooltip.element);
            expect(Tooltip.element).toBeHidden();
        });

        it('should show tooltip when cursor hovers over element within a second', function() {
            jasmine.clock().install();
            let tooltip = Tooltip.load(this.element);
            Helper.dispatchMouseEvent('mouseenter', this.element);
            jasmine.clock().tick(1000);
            expect(Tooltip.element).toBeVisible();
            jasmine.clock().uninstall();
        });

        it('should not show tooltip when cursor hovers over element with disable-tooltip class', function() {
            jasmine.clock().install();
            let tooltip = Tooltip.load(this.element);
            Helper.dispatchMouseEvent('mouseenter', this.element);
            jasmine.clock().tick(90); // simulate async setting of class
            this.element.classList.add('disable-tooltip');
            jasmine.clock().tick(1000);
            expect(Tooltip.element).toBeHidden();
            jasmine.clock().uninstall();
        });

        it('should not show tooltip when cursor hovers over disabled element', function() {
            jasmine.clock().install();
            let tooltip = Tooltip.load(this.element);
            Helper.dispatchMouseEvent('mouseenter', this.element);
            jasmine.clock().tick(90); // simulate async setting of class
            this.element.setAttribute('disabled', '');
            jasmine.clock().tick(1000);
            expect(Tooltip.element).toBeHidden();
            jasmine.clock().uninstall();
        });

        it('should not show tooltip when cursor hovers over an element that is a child of a disabled element', function() {
            jasmine.clock().install();
            let tooltip = Tooltip.load(this.element);
            Helper.dispatchMouseEvent('mouseenter', this.element);
            jasmine.clock().tick(90); // simulate async setting of class
            this.element.parentNode.setAttribute('disabled', '');
            jasmine.clock().tick(1000);
            expect(Tooltip.element).toBeHidden();
            jasmine.clock().uninstall();
        });

        it('should hide tooltip when cursor moves onto and then off of element', function() {
            jasmine.clock().install();
            let tooltip = Tooltip.load(this.element);
            Helper.dispatchMouseEvent('mouseenter', this.element);
            jasmine.clock().tick(1000);
            Helper.dispatchMouseEvent('mouseleave', this.element);
            expect(Tooltip.element).toBeHidden();
            jasmine.clock().uninstall();
        });

        it('should hide tooltip when mouse clicks on element', function() {
            jasmine.clock().install();
            let tooltip = Tooltip.load(this.element);
            Helper.dispatchMouseEvent('mouseenter', this.element);
            jasmine.clock().tick(1000);
            Helper.dispatchMouseEvent('click', this.element);
            expect(Tooltip.element).toBeHidden();
            jasmine.clock().uninstall();
        });
    });
});
