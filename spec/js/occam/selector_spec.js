"use strict";

import Helper from '../helper';

import Selector from '../../../public/js/occam/selector';

describe('Selector', () => {
    beforeEach(function() {
        jasmine.addMatchers(DOMCustomMatchers);
        jasmine.addMatchers(Helper.VisibilityMatchers);

        // Create a simple document with
        // a div.content and a filled in <select>

        // A random item is selected.
        let total = 5;
        let select = document.createElement("select");
        let selectedIndex = Helper.randomInteger(total);
        select.classList.add("selector");
        for (let i = 0; i < total; i++) {
            let option = document.createElement("option");
            option.textContent = "option " + i;
            select.appendChild(option);
            if (i == selectedIndex) {
                option.setAttribute('selected', '');
            }
        }

        let selectNone = document.createElement("select");
        selectNone.classList.add("selector");
        for (let i = 0; i < total; i++) {
            let option = document.createElement("option");
            option.textContent = "option " + i;
            selectNone.appendChild(option);
        }

        let data = [];
        let selectFull = document.createElement("select");
        selectFull.classList.add("selector");
        for (let i = 0; i < total; i++) {
            let option = document.createElement("option");
            let name = "option " + i;
            let type = Helper.randomString();
            option.textContent = name;
            data.push({type: type, name: name});
            option.setAttribute('data-object-type', type);
            selectFull.appendChild(option);
            if (i == selectedIndex) {
                option.setAttribute('selected', '');
            }
        }

        let content = document.createElement("div");
        content.classList.add("content");
        document.body.appendChild(content);
        content.appendChild(select);
        content.appendChild(selectNone);
        content.appendChild(selectFull);

        this.data = data;
        this.total = total;
        this.selectedIndex = selectedIndex
        this.select = select;
        this.selectNone = selectNone;
        this.selectFull = selectFull;
    });

    describe('.constructor', () => {
        it('should throw TypeError when element is not passed', function() {
            expect( () => new Selector() ).toThrowError(TypeError);
        });

        it('should throw TypeError when element is not HTMLElement', function() {
            expect( () => new Selector("select.selector") ).toThrowError(TypeError);
        });

        it('should set selectElement when passed the created element', function() {
            let selectorA = Selector.load(this.select);
            let selectorB = new Selector(selectorA.element);
            expect(selectorB.selectElement).toEqual(selectorA.selectElement);
        });

        it('should assign data-loaded-index attribute', function() {
            let selector = new Selector(this.select);
            expect(selector.element).toHaveAttribute('data-loaded-index');
        });

        it('should be closed by default', function() {
            let selector = Selector.load(this.select);
            expect(selector.dropdown).toBeHidden();
        });
    });

    describe('.load', () => {
        it('should throw TypeError when element is not passed', function() {
            expect( () => Selector.load() ).toThrowError(TypeError);
        });

        it('should throw TypeError when element is not an HTMLElement', function() {
            expect( () => Selector.load("select.selector") ).toThrowError(TypeError);
        });

        it('should instantiate a new Selector on the first invocation', function() {
            let spy = spyOn(Selector.prototype, 'bindEvents').and.callThrough();

            let selector1 = Selector.load(this.select);
            expect(selector1.bindEvents).toHaveBeenCalled();
        });

        it('should return the instantiated Selector on the second invocation', function() {
            let selector1 = Selector.load(this.select);

            let spy = spyOn(Selector.prototype, 'bindEvents').and.callThrough();
            let selector2 = Selector.load(this.select);
            expect(selector2.bindEvents).not.toHaveBeenCalled();
        });

        it('should hide dropdown initially', function() {
            let selector = Selector.load(this.select);
            expect(selector.dropdown).toBeHidden();
        });

        it('should ensure first option is selected by default via attribute', function() {
            let selector = Selector.load(this.selectNone);
            let index = 0;
            let element = selector.selectElement.querySelector('option:nth-of-type(' + (index + 1) + ')');
            expect(element).toHaveAttribute('selected');
        });

        it('should copy the hidden attribute from SELECT tags', function() {
            let select = this.select;
            select.setAttribute('hidden', '');

            let selector = Selector.load(this.select);
            let element = selector.element;
            expect(element).toHaveAttribute('hidden');
        });

        it('should copy the title attribute from SELECT tags', function() {
            let select = this.select;
            select.setAttribute('title', 'boo');

            let selector = Selector.load(this.select);
            let element = selector.element;
            expect(element).toHaveAttribute('title', 'boo');
        });

        it('should copy the aria-label attribute from SELECT tags', function() {
            let select = this.select;
            select.setAttribute('aria-label', 'foo');

            let selector = Selector.load(this.select);
            let element = selector.element;
            expect(element).toHaveAttribute('aria-label', 'foo');
        });

        it('should copy the data-tooltip-text attribute from SELECT tags', function() {
            let select = this.select;
            select.setAttribute('data-tooltip-text', 'foo');

            let selector = Selector.load(this.select);
            let element = selector.element;
            expect(element).toHaveAttribute('data-tooltip-text', 'foo');
        });

        it('should copy the data-icon-base-url attribute from SELECT tags', function() {
            let select = this.select;
            select.setAttribute('data-icon-base-url', 'foo');

            let selector = Selector.load(this.select);
            let element = selector.element;
            expect(element).toHaveAttribute('data-icon-base-url', 'foo');
        });

        it('should copy the data-icon-color attribute from SELECT tags', function() {
            let select = this.select;
            select.setAttribute('data-icon-color', 'foo');

            let selector = Selector.load(this.select);
            let element = selector.element;
            expect(element).toHaveAttribute('data-icon-color', 'foo');
        });

        it('should copy the data-icon-hover-color attribute from SELECT tags', function() {
            let select = this.select;
            select.setAttribute('data-icon-hover-color', 'foo');

            let selector = Selector.load(this.select);
            let element = selector.element;
            expect(element).toHaveAttribute('data-icon-hover-color', 'foo');
        });

        it('should copy the data-icon-disabled-color attribute from SELECT tags', function() {
            let select = this.select;
            select.setAttribute('data-icon-disabled-color', 'foo');

            let selector = Selector.load(this.select);
            let element = selector.element;
            expect(element).toHaveAttribute('data-icon-disabled-color', 'foo');
        });

        it('should copy the hidden attribute from OPTION tags', function() {
            let select = this.select;
            let option = select.querySelector("option:first-child");
            option.setAttribute('hidden', '');

            let selector = Selector.load(this.select);
            let element = selector.dropdown.querySelector('*:first-child');
            expect(element).toHaveAttribute('hidden');
        });

        it('should copy the class attribute from OPTION tags', function() {
            let select = this.select;
            let option = select.querySelector("option:first-child");
            option.classList.add('foo');

            let selector = Selector.load(this.select);
            let element = selector.dropdown.querySelector('*:first-child h2');
            expect(element).toHaveClass('foo');
        });

        it('should use the data-i18n attribute from OPTION tags as displayed text', function() {
            let select = this.select;
            let option = select.querySelector("option:first-child");
            option.setAttribute('data-i18n', 'foo');

            let selector = Selector.load(this.select);
            let element = selector.dropdown.querySelector('*:first-child h2');
            expect(element.textContent).toEqual('foo');
        });

        it('should preserve with data-i18n-default when the data-i18n attribute is set on OPTION tags', function() {
            let select = this.select;
            let option = select.querySelector("option:first-child");
            option.setAttribute('data-i18n', 'foo');

            let selector = Selector.load(this.select);
            let element = selector.dropdown.querySelector('*:first-child h2');
            expect(element).toHaveAttribute('data-i18n-default', option.textContent);
        });

        it('should apply OPTION data-class to created dropdown item class', function() {
            let select = this.select;
            let option = select.querySelector("option:first-child");
            option.setAttribute('data-class', 'foo');

            let selector = Selector.load(this.select);
            let element = selector.dropdown.querySelector('*:first-child h2');
            expect(element).toHaveClass('foo');
        });

        it('should create an IMG when data-icon-url is specified on an OPTION', function() {
            let select = this.select;
            let option = select.querySelector("option:first-child");
            option.setAttribute('data-icon-url', 'http://example.com/foo');

            let selector = Selector.load(this.select);
            let element = selector.dropdown.querySelector('*:first-child img');
            expect(element).toBeDocumentNode();
        });

        it('should create a hover IMG when data-icon-url is specified on an OPTION', function() {
            let select = this.select;
            let option = select.querySelector("option:first-child");
            option.setAttribute('data-icon-url', 'http://example.com/foo');

            let selector = Selector.load(this.select);
            let element = selector.dropdown.querySelector('*:first-child img.hover');
            expect(element).toBeDocumentNode();
        });

        it('should create a disabled IMG when data-icon-url is specified on an OPTION', function() {
            let select = this.select;
            let option = select.querySelector("option:first-child");
            option.setAttribute('data-icon-url', 'http://example.com/foo');

            let selector = Selector.load(this.select);
            let element = selector.dropdown.querySelector('*:first-child img.disabled');
            expect(element).toBeDocumentNode();
        });

        it('should set source for IMG to data-icon-url when specified on an OPTION', function() {
            let select = this.select;
            let option = select.querySelector("option:first-child");
            option.setAttribute('data-icon-url', 'http://example.com/foo');

            let selector = Selector.load(this.select);
            let element = selector.dropdown.querySelector('*:first-child img');
            expect(element.src).toEqual(option.getAttribute('data-icon-url'));
        });

        it('should set source for hover IMG to data-icon-url when specified on an OPTION', function() {
            let select = this.select;
            let option = select.querySelector("option:first-child");
            option.setAttribute('data-icon-url', 'http://example.com/foo');

            let selector = Selector.load(this.select);
            let element = selector.dropdown.querySelector('*:first-child img.hover');
            expect(element.src).toEqual(option.getAttribute('data-icon-url'));
        });

        it('should set source for disabled IMG to data-icon-url when specified on an OPTION', function() {
            let select = this.select;
            let option = select.querySelector("option:first-child");
            option.setAttribute('data-icon-url', 'http://example.com/foo');

            let selector = Selector.load(this.select);
            let element = selector.dropdown.querySelector('*:first-child img.disabled');
            expect(element.src).toEqual(option.getAttribute('data-icon-url'));
        });

        it('should set source for IMG using data-icon-base-url on SELECT and data-icon-color on OPTION', function() {
            let select = this.select;
            select.setAttribute('data-icon-base-url', 'http://example.com/foo/');
            select.setAttribute('data-icon-color', 'ABCDEF');
            let option = select.querySelector("option:first-child");
            option.setAttribute('data-icon-url', '/bar.svg');

            let selector = Selector.load(this.select);
            let element = selector.dropdown.querySelector('*:first-child img');
            expect(element.src).toEqual('http://example.com/foo/ABCDEF/bar.svg');
        });

        it('should set source for hover IMG using data-icon-base-url on SELECT and data-icon-color on OPTION', function() {
            let select = this.select;
            select.setAttribute('data-icon-base-url', 'http://example.com/foo/');
            select.setAttribute('data-icon-color', 'ABCDEF');
            let option = select.querySelector("option:first-child");
            option.setAttribute('data-icon-url', '/bar.svg');

            let selector = Selector.load(this.select);
            let element = selector.dropdown.querySelector('*:first-child img.hover');
            expect(element.src).toEqual('http://example.com/foo/ABCDEF/bar.svg');
        });

        it('should set source for disabled IMG using data-icon-base-url on SELECT and data-icon-color on OPTION', function() {
            let select = this.select;
            select.setAttribute('data-icon-base-url', 'http://example.com/foo/');
            select.setAttribute('data-icon-color', 'ABCDEF');
            let option = select.querySelector("option:first-child");
            option.setAttribute('data-icon-url', '/bar.svg');

            let selector = Selector.load(this.select);
            let element = selector.dropdown.querySelector('*:first-child img.disabled');
            expect(element.src).toEqual('http://example.com/foo/ABCDEF/bar.svg');
        });

        it('should set source for hover IMG using data-icon-base-url on SELECT and data-icon-hover-color on OPTION', function() {
            let select = this.select;
            select.setAttribute('data-icon-base-url', 'http://example.com/foo/');
            select.setAttribute('data-icon-color', 'ABCDEF');
            select.setAttribute('data-icon-hover-color', 'FEDCBA');
            let option = select.querySelector("option:first-child");
            option.setAttribute('data-icon-url', '/bar.svg');

            let selector = Selector.load(this.select);
            let element = selector.dropdown.querySelector('*:first-child img.hover');
            expect(element.src).toEqual('http://example.com/foo/FEDCBA/bar.svg');
        });

        it('should set source for disabled IMG using data-icon-base-url on SELECT and data-icon-disabled-color on OPTION', function() {
            let select = this.select;
            select.setAttribute('data-icon-base-url', 'http://example.com/foo/');
            select.setAttribute('data-icon-color', 'ABCDEF');
            select.setAttribute('data-icon-disabled-color', 'FEDCBA');
            let option = select.querySelector("option:first-child");
            option.setAttribute('data-icon-url', '/bar.svg');

            let selector = Selector.load(this.select);
            let element = selector.dropdown.querySelector('*:first-child img.disabled');
            expect(element.src).toEqual('http://example.com/foo/FEDCBA/bar.svg');
        });
    });

    describe('.loadAll', () => {
        it('should throw TypeError when element is not passed', function() {
            expect( () => Selector.loadAll() ).toThrowError(TypeError);
        });

        it('should throw TypeError when element is not an HTMLElement', function() {
            expect( () => Selector.loadAll("select.selector") ).toThrowError(TypeError);
        });

        it('should instantiate each discovered selector', function() {
            let spy = spyOn(Selector, 'load').and.callThrough();

            Selector.loadAll(document.body);
            expect(Selector.load).toHaveBeenCalled();
        });
    });

    describe('#selected', () => {
        it('should return an HTMLElement', function() {
            let selector = Selector.load(this.select);
            let item = selector.selected();
            expect(item).toEqual(jasmine.any(HTMLElement));
        });

        it('should return an element within the dropdown list', function() {
            let selector = Selector.load(this.select);
            let item = selector.selected();
            expect(item).toBeChildOf(selector.dropdown);
        });

        it('should return the currently selected item', function() {
            let selector = Selector.load(this.select);
            let item = selector.selected();
            expect(item).toBeNthChild(this.selectedIndex);
        });
    });

    describe('#clear', () => {
        it('should result in the dropdown containing no visible elements', function() {
            let selector = Selector.load(this.select);
            selector.clear();
            expect(selector.dropdown.querySelectorAll('button:not([hidden])').length).toEqual(0);
        });
    });

    describe('#loading', () => {
        it('should add a waiting class to the element', function() {
            let selector = Selector.load(this.select);
            selector.loading();
            expect(selector.element.classList.contains('waiting')).toEqual(true);
        });

        it('should contain only a loading element', function() {
            let selector = Selector.load(this.select);
            selector.loading();
            expect(selector.element.querySelectorAll("*").length).toEqual(1);
            expect(selector.element.querySelectorAll("*")[0].classList.contains('loading')).toEqual(true);
        });
    });

    describe('#loaded', () => {
        it('should remove the waiting class from the element', function() {
            let selector = Selector.load(this.select);
            selector.loaded();
            expect(selector.element.classList.contains('waiting')).toEqual(false);
        });

        it('should contain nothing', function() {
            let selector = Selector.load(this.select);
            selector.loaded();
            expect(selector.element.querySelectorAll("*").length).toEqual(0);
        });
    });

    describe('#items', () => {
        it('should return the number of items as are in the select', function() {
            let selector = Selector.load(this.select);
            let items = selector.items();
            expect(items.length).toEqual(this.total);
        });

        it('should return a NodeList', function() {
            let selector = Selector.load(this.select);
            let items = selector.items();
            expect(items instanceof NodeList).toEqual(true);
        });

        it('should return a list of elements that are within the dropdown', function() {
            let selector = Selector.load(this.select);
            let items = selector.items();
            items.forEach( (item) => {
                expect(item).toBeChildOf(selector.dropdown);
            });
        });
    });

    describe('#indexFor', () => {
        it('should return the correct index when given a valid dropdown element', function() {
            let selector = Selector.load(this.select);
            let index = Math.floor(Math.random() * this.total);
            let element = selector.dropdown.querySelector('*:nth-of-type(' + (index + 1) + ')');
            let returnedIndex = selector.indexFor(element);
            expect(returnedIndex).toEqual(index);
        });

        it('should pass through the index if it is a number', function() {
            let selector = Selector.load(this.select);
            let index = Helper.randomInteger(this.total);
            let returnedIndex = selector.indexFor(index);
            expect(returnedIndex).toEqual(index);
        });

        it('should return the valid index when passed data with just a type', function() {
            let selector = Selector.load(this.select);
            let index = Helper.randomInteger(this.total);
            let returnedIndex = selector.indexFor({
                type: this.data[index].name
            });
            expect(returnedIndex).toEqual(index);
        });

        it('should return the valid index when passed data with type and name', function() {
            let selector = Selector.load(this.selectFull);
            let index = Helper.randomInteger(this.total);
            let returnedIndex = selector.indexFor({
                name: this.data[index].name,
                type: this.data[index].type
            });
            expect(returnedIndex).toEqual(index);
        });
    });

    describe('#disable', () => {
        it('should set the disabled attribute', function() {
            let selector = Selector.load(this.select);
            selector.disable();
            expect(selector.element).toHaveAttribute('disabled');
        });
    });

    describe('#enable', () => {
        it('should clear the disabled attribute', function() {
            let selector = Selector.load(this.select);
            selector.enable();
            expect(selector.element).not.toHaveAttribute('disabled');
        });
    });

    describe('#elementFor', () => {
        it('should pass through the element when given a valid dropdown element', function() {
            let selector = Selector.load(this.select);
            let index = Math.floor(Math.random() * this.total);
            let element = selector.dropdown.querySelector('*:nth-of-type(' + (index + 1) + ')');
            let returnedElement = selector.elementFor(element);
            expect(returnedElement).toBe(element);
        });

        it('should return the corrent element when given a valid index', function() {
            let selector = Selector.load(this.select);
            let index = Helper.randomInteger(this.total);
            let element = selector.dropdown.querySelector('*:nth-of-type(' + (index + 1) + ')');
            let returnedElement = selector.elementFor(index);
            expect(returnedElement).toBe(element);
        });

        it('should return the corrent element when passed data with just a type', function() {
            let selector = Selector.load(this.select);
            let index = Helper.randomInteger(this.total);
            let element = selector.dropdown.querySelector('*:nth-of-type(' + (index + 1) + ')');
            let returnedElement = selector.elementFor({
                type: this.data[index].name
            });
            expect(returnedElement).toBe(element);
        });

        it('should return the correct element when passed data with type and name', function() {
            let selector = Selector.load(this.selectFull);
            let index = Helper.randomInteger(this.total);
            let element = selector.dropdown.querySelector('*:nth-of-type(' + (index + 1) + ')');
            let returnedElement = selector.elementFor({
                name: this.data[index].name,
                type: this.data[index].type
            });
            expect(returnedElement).toBe(element);
        });

        it('should return null when passed an invalid index', function() {
            let selector = Selector.load(this.selectFull);
            let returnedElement = selector.elementFor(-1);
            expect(returnedElement).toBeNull();
        });

        it('should return null when passed an index that is out of range', function() {
            let selector = Selector.load(this.selectFull);
            let returnedElement = selector.elementFor(this.total);
            expect(returnedElement).toBeNull();
        });
    });

    describe('#open', () => {
        it('should make the dropdown visible', function() {
            let selector = Selector.load(this.selectFull);
            selector.open();
            expect(selector.dropdown).toBeVisible();
        });

        it('should set the aria-expanded attribute on the dropdown', function() {
            let selector = Selector.load(this.selectFull);
            selector.open();
            expect(selector.element).toHaveAttribute('aria-expanded', 'true');
        });

        it('should not make the dropdown visible if loading', function() {
            let selector = Selector.load(this.selectFull);
            selector.loading();
            selector.open();
            expect(selector.dropdown).toBeHidden();
        });

        it('should focus on the selected item', function() {
            let selector = Selector.load(this.selectFull);

            let selected = selector.selected();
            let spy = spyOn(selected, 'focus').and.callThrough();

            selector.open();
            expect(selected.focus).toHaveBeenCalled();
        });

        it('should remove the tabindex from the selected item', function() {
            let selector = Selector.load(this.selectFull);
            selector.open();
            expect(selector.selected()).toHaveAttribute('tabindex', '-1');
        });

        it('should focus on the dropdown if nothing is selected', function() {
            let selector = Selector.load(this.select);
            selector.select(-1); // Unselect

            let spy = spyOn(selector.dropdown, 'focus').and.callThrough();

            selector.open();
            expect(selector.dropdown.focus).toHaveBeenCalled();
        });
    });

    describe('#infoFor', () => {
        it('should return an object with the correct type field', function() {
            let selector = Selector.load(this.selectFull);
            let index = Helper.randomInteger(this.total);
            let info = selector.infoFor(index);
            expect(info.type).toEqual(this.data[index].type);
        });

        it('should return an object with the correct name field', function() {
            let selector = Selector.load(this.selectFull);
            let index = Helper.randomInteger(this.total);
            let info = selector.infoFor(index);
            expect(info.name).toEqual(this.data[index].name);
        });
    });

    describe('Accessibility', () => {
        it('should open when clicked', function() {
            let selector = Selector.load(this.select);
            Helper.dispatchMouseEvent('click', selector.element);
            expect(selector.dropdown).toBeVisible();
        });

        it('should open when down is pressed', function() {
            let selector = Selector.load(this.select);
            Helper.dispatchKeyEvent('keydown', selector.element, {
              code: 40
            });
            expect(selector.dropdown).toBeVisible();
        });

        it('should go to the next item when down is pressed and open', function() {
            let selector = Selector.load(this.select);
            selector.select(0);
            let index = 1;
            let element = selector.dropdown.querySelector('*:nth-of-type(' + (index + 1) + ')');
            let spy = spyOn(element, 'focus').and.callThrough();
            selector.open();
            Helper.dispatchKeyEvent('keydown', selector.selected(), {
              code: 40
            });
            expect(element.focus).toHaveBeenCalled();
        });

        it('should go to the previous item when up is pressed and open', function() {
            let selector = Selector.load(this.select);
            selector.select(1);
            let index = 0;
            let element = selector.dropdown.querySelector('*:nth-of-type(' + (index + 1) + ')');
            let spy = spyOn(element, 'focus').and.callThrough();
            selector.open();
            Helper.dispatchKeyEvent('keydown', selector.selected(), {
              code: 38
            });
            expect(element.focus).toHaveBeenCalled();
        });

        it('should remain on the first item when up is pressed and open', function() {
            let selector = Selector.load(this.select);
            selector.select(0);
            let index = 0;
            let element = selector.dropdown.querySelector('*:nth-of-type(' + (index + 1) + ')');
            let spy = spyOn(element, 'focus').and.callThrough();
            selector.open();
            Helper.dispatchKeyEvent('keydown', selector.selected(), {
              code: 38
            });
            expect(element.focus).toHaveBeenCalled();
        });

        it('should remain on the last item when down is pressed and open', function() {
            let selector = Selector.load(this.select);
            selector.select(this.total - 1);
            let index = this.total - 1;
            let element = selector.dropdown.querySelector('*:nth-of-type(' + (index + 1) + ')');
            let spy = spyOn(element, 'focus').and.callThrough();
            selector.open();
            Helper.dispatchKeyEvent('keydown', selector.selected(), {
              code: 40
            });
            expect(element.focus).toHaveBeenCalled();
        });

        it('should close the dropdown when escape is pressed when open', function() {
            let selector = Selector.load(this.select);
            selector.open();
            Helper.dispatchKeyEvent('keydown', selector.selected(), {
              code: 27
            });
            expect(selector.dropdown).toBeHidden();
        });

        it('should focus on the first item when down is pressed and open with no selection', function() {
            let selector = Selector.load(this.select);
            selector.select(-1);
            let index = 0;
            let element = selector.dropdown.querySelector('*:nth-of-type(' + (index + 1) + ')');
            let spy = spyOn(element, 'focus').and.callThrough();
            selector.open();
            Helper.dispatchKeyEvent('keydown', selector.dropdown, {
              code: 40
            });
            expect(element.focus).toHaveBeenCalled();
        });

        it('should focus on the last item when up is pressed and open with no selection', function() {
            let selector = Selector.load(this.select);
            selector.select(-1);
            let index = this.total - 1;
            let element = selector.dropdown.querySelector('*:nth-of-type(' + (index + 1) + ')');
            let spy = spyOn(element, 'focus').and.callThrough();
            selector.open();
            Helper.dispatchKeyEvent('keydown', selector.dropdown, {
              code: 38
            });
            expect(element.focus).toHaveBeenCalled();
        });

        it('should close dropdown when escape is pressed and open with no selection', function() {
            let selector = Selector.load(this.select);
            selector.select(-1);
            selector.open();
            Helper.dispatchKeyEvent('keydown', selector.dropdown, {
              code: 27
            });
            expect(selector.dropdown).toBeHidden();
        });

        it('should close dropdown when item is clicked', function() {
            let selector = Selector.load(this.select);
            selector.open();
            let index = Helper.randomInteger(this.total);
            let element = selector.dropdown.querySelector('*:nth-of-type(' + (index + 1) + ')');
            Helper.dispatchMouseEvent('click', element);
            expect(selector.dropdown).toBeHidden();
        });

        it('should select item when item is clicked', function() {
            let selector = Selector.load(this.select);
            selector.open();
            let index = Helper.randomInteger(this.total);
            let element = selector.dropdown.querySelector('*:nth-of-type(' + (index + 1) + ')');
            Helper.dispatchMouseEvent('click', element);
            expect(selector.selected()).toBe(element);
        });
    });
});
