"use strict";

console.log("Formatting JSON results");

const fs = require('fs');

let testjson = fs.readFileSync('spec/js/karma-result.json');
let testdata = JSON.parse(testjson);

let results = {};

results.groups = {};
results.groups.unit = {};
results.groups.unit.name = "Unit Tests";

results.metadata = {};

let failed = false;

// Gather Browsers
Object.keys(testdata.browsers).forEach( (key) => {
    let browserInfo = testdata.browsers[key];

    results.metadata["Browser " + key + " UserAgent"] = browserInfo.fullName;
    results.metadata["Browser " + key + " name"] = browserInfo.name;

    testdata.result[key].forEach( (test) => {
        let currentGroup = results.groups.unit;
        test.suite.forEach( (tag) => {
            if (!currentGroup.describes) {
                currentGroup.describes = {};
            }

            if (currentGroup.describes[tag] === undefined) {
                currentGroup.describes[tag] = {};
                currentGroup.describes[tag].name = tag;

                if (currentGroup.describes[tag].name[0] === '.') {
                    currentGroup.describes[tag].type = "function";
                }
                else {
                    currentGroup.describes[tag].type = "class";
                }
            }

            currentGroup = currentGroup.describes[tag];
        });

        if (!currentGroup.tests) {
            currentGroup.tests = [];
        }

        let testResult = {};

        testResult.it = test.description;
        testResult.time = test.time;
        testResult.assertions = test.executedExpectationsCount || 0;
        if (test.skipped) {
            testResult.type = "skipped";
        }
        else if (test.success) {
            testResult.type = "passed";
        }
        else {
            testResult.type = "failed";
            failed = true;
        }

        if (test.id) {
            testResult.number = parseInt(test.id.substring(4));
        }

        currentGroup.tests.push(testResult);
    });
});

console.log("Formatting JSON results: writing `spec/js/tests.json'.");
let output = JSON.stringify(results);
fs.writeFileSync('spec/js/tests.json', output);

if (failed) {
    process.exit(-1);
}
