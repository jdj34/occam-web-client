# Modify yard-parser to coax some route names into better forms

require 'yard'

module YARD
  module CodeObjects
    class RouteObject < MethodObject
      if not RouteObject.method_defined?(:old_real_name)
        alias_method :old_real_name, :real_name
        def real_name
          ret = old_real_name

          if old_real_name
            # Replace OBJECT_ROUTE_REGEX
            ret.gsub!("\#{OBJECT_ROUTE_REGEX}", "/:id/:revision?")

            # Replace regular expression stuff
            ret.gsub!("%r{", "")
            ret.gsub!(/[}]$/, "")

            # Replace final '/?' which should be obvious
            ret.gsub!(/#{Regexp.escape("/?")}$/, "")

            # Replace fields
            ret.gsub!(/#{Regexp.escape("(?<")}([a-zA-Z_]+)#{Regexp.escape(">")}[^)]+#{Regexp.escape(")")}/, ':\1')
          end

          ret
        end
      end
    end
  end
end
